# tutorials

We have prepared two tutorials for [GROMACS constant pH MD](https://gitlab.com/gromacs-constantph/constantph):
1. [Preparing and running single amino acid](https://gitlab.com/gromacs-constantph/tutorials/-/blob/main/1_ASP_simulations/tutorial.md)
2. [Protein titration](https://gitlab.com/gromacs-constantph/tutorials/-/blob/main/2_1CVO_titration/tutorial.md)

In these tutorials we cover the whole process of setting up and running constant pH MD with GROMACS. First, we demonstrate how the $`V^{\text{MM}}`$ fits are obtained for single amino acid. Next, we show how to run protein titrations using GROMACS constant pH. We assume that the user is familiar with basic GROMACS commands and standart $`\tt{.mdp}`$ options. Otherwise, we strongly recommend the user to follow the [GROMACS tutorials](https://tutorials.gromacs.org/) first.
