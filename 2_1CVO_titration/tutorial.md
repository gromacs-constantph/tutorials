# Protein titration with Constant pH MD

In this tutorial we will perform the titration of cardiotoxin V (PDBID: [1CVO](https://www.rcsb.org/structure/1CVO))[1] using the GROMACS constant pH code [2]. First, we discuss the system, next we set up simulations, and finally, we analyze the results.

## System preparation
The deposited structure of [cardiotoxin V](https://www.rcsb.org/structure/1CVO) has 2 conformers. We are going to work with state 1, which can be found in ``tutorial_files/parameterisation/input_files`` folder. Overall, the protein has 4 titratable residues: His-5, Glu-17, Asp-42 and Asp-59:

![1CVO structure](2_1CVO_titration/tutorial_files/output_files/1cvo.png "Cardiotoxin V structure. Titratable groups are shown in sticks representation.")

The system preparation is standard. We first create the topology, change the box, add waters, add ions and add buffers to keep the box neutral. Next, we run energy minimization and, finally, we run 2 equilibration steps in NVT and NPT ensembles. All the files needed to prepare system are in ``tutorial_files//input_files`` folder. The details, can be found in the first tutorial. We recommend you to do all the steps yourself, but you can find all the output files in ``tutorial_files//output_files`` folder.

**Note:** Make sure that after adding waters to the protein system with `gmx solvate` no water molecules appeared in the hydrophobic regions of the protein.

With the prepared system we are ready to set up the titration. First, we need to prepare the $`\tt{.mdp}`$ file. The structure of constant pH $`\tt{.mdp}`$ file has been described in details in the [first tutorial](https://gitlab.com/gromacs-constantph/tutorials/-/blob/main/1_ASP_simulations/tutorial.md). Here, we provide the coefficients for the correction potential $`V^{\text{MM}}`$ for ASP, GLU, and BUF:

```
	ASP:   487.794 -2123.99    3574.07  -3194.59  1539.43  -353.879  -555.935  45.9833
	GLU:   247.368  -761.848    776.508  -557.542  393.084 -163.762  -556.75   25.8878
	BUF: -2391.04   8581.51  -10755.2    4646.51   382.852 -103.978 -1254.42  575.647
```

It is important to focus on the HIS group type description. Histidine represents an amino acid with chemically coupled protonation sites, thus, it has three different physically possible protonation states. The group type entry is organized as follows:

```
	lambda-dynamics-group-type3-name                       = HIS
	lambda-dynamics-group-type3-n-states                   = 3
	lambda-dynamics-group-type3-state-0-charges            = 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0
	lambda-dynamics-group-type3-state-1-charges            = -0.05 0.19 0.13 0.19 -0.51 0.44 -0.51 0.44 0.32 0.18
	lambda-dynamics-group-type3-state-2-charges            = -0.08 -0.05 0.09 0.22 -0.36 0.32 -0.7 0.0 0.25 0.13
	lambda-dynamics-group-type3-state-3-charges            = -0.09 0.22 0.1 -0.05 -0.7 0.0 -0.36 0.32 0.25 0.13
	lambda-dynamics-group-type3-state-1-reference-pka      = pH
	lambda-dynamics-group-type3-state-2-reference-pka      = 6.53
	lambda-dynamics-group-type3-state-3-reference-pka      = 6.92
	lambda-dynamics-group-type3-state-1-dvdl-coefficients  = 111.81 1664.17 4823.68 7652. 7275.68 5413.33 1032.96 -67.9617 -513.523 -5797.74 -13766. -17578. -14260.9 -4443.94 147.538 1052.93 8212.57 15507.5 15458.9 6961.06 103.793 -958.673 -5322.38 -7712.91 -4967.13 -370.456 86.3837 828.127 993.008 165.269 404.336 575.587 175.358 -452.864 -185.883 82.2376
	lambda-dynamics-group-type3-state-2-dvdl-coefficients  = 405.453 1821.51 5606.96 8629.2 9300.07 5930.92 2288.67 83.0846 -1618.13 -6474.98 -15890.7 -20573. -16484.3 -7244.69 -391.848 2584.02 9057.04 17210.3 17341.3 9086.71 803.537 -1905.7 -5668.04 -8091.84 -5460.93 -793.444 383.372 960.913 1078.56 219.006 269.28 406.29 146.361 -130.936 -292.352 -28.6856
	lambda-dynamics-group-type3-state-3-dvdl-coefficients  = 547.233 2656.63 7329.04 10522.1 9168.62 5334.47 834.145 -275.752 -1955.26 -7820.49 -17129.8 -19230.1 -13253.7 -3151.69 829.705 2464.2 7613.44 12620.3 10868.9 3724.88 -968.572 -1139.63 -2258.09 -2598. -1304.56 668.634 13.6195 -281.129 -249.034 -277.262 -17.2137 2.59824 -26.488 285.785 230.838 -171.027
```

Note, that ``state-0-charges`` are all zeros. This corresponds to an artificial common state, which is unphysical and never achieved in simulations due to constraints on $`\lambda`$-coordinates. Please, check out the [manual](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md) for the detailed description.

Also note, that we set ``lambda-dynamics-group-type3-state-1-reference-pka`` as ``pH``. This is needed to automatically set up titration runs and will be discussed in the following sections.

The coefficients for $`V^{\text{MM}}`$ are also provided in the above definition. More details on parametrizing a multisite titratable group can be found in the original manuscript [2].

## Setting up simulations
Next, we'll run the full titration of cardiotoxin V in the pH range from 1 to 8. To automatically set up all those simulations we will use a python script ``create_titration.py``. If you want to test that your system is setup correctly, and all the input options are fine, please make short test runs for your setup before proceeding to the full titration.
To get a help, run it with ``-h`` flag:

```
	Prepare files for titration simulations. The code will create directories,
	modify .mdp file, run grompp and if needed mdrun for specified pH values and
	for the desired number of replicas. Also, if some pKa values have to be set to
	pH, this can be specified by setting the corresponding [lambda-dynamics-group-
	type1-state-1-reference-pka] to pH. The code will automtically recognize it
	and set the proper pKa.

	optional arguments:
	  -h, --help            show this help message and exit
	  -f MDP, --mdp MDP     Input .mdp file
	  -c STRUCT, --struct STRUCT
	                        Input structure file
	  -r REF, --ref REF     Input reference structure
	  -p TOP, --top TOP     Input topology file
	  -n INDEX, --index INDEX
	                        Input index file
	  -pH PHRANGE, --pHrange PHRANGE
	                        pH values at which we run simulations. Those can be
	                        provided in the form of range "a:b:d" or as a list
	                        "{a,b,c,..,}"
	  -nr NREP, --nrep NREP
	                        Number of replicas we run for each pH
	  -o OUT, --out OUT     Prefix name
	  -gmx GMXPATH, --gmxpath GMXPATH
	                        GMXPATH. Default is gmx
	  -rc GMXRUN, --gmxrun GMXRUN
	                        Run gmx. Default is None

```

The script uses the basis $`\tt{.mdp}`$ file and modifies it for the desired pH, creates folders for all replicas, runs ``grompp`` and ``mdrun``. To run a simulation, we need to provide the exact command to be run in each folder under the flag ``--gmxrun`` (either ``mdrun`` command, or commands needed to submit a batch job script).
We will run 15 equidistantly separated pH points in range from 1 to 8. For each pH we will run 2 replicas. To set up all these simulations we need the command:

```
	python create_titration.py -f md_cph.mdp -c 8_npt.gro -p topol.top -n index.ndx -pH "1:8.5:0.5" -nr 2 -gmx gmx -rc "gmx mdrun -deffnm run"
```

In case you are running on cluster or you have GROMACS installed under a nonstandard name, you should change the command above. Now all the simulations should be running.

## Analysis
Our analysis is based on ``analyse_titration.py`` script. It computes the predicted pKa values for the protein and plots titration curves. 
Before using the script, make sure that all titration folders contain `cphmd-coord-*.xvg` files with the $`\lambda`$-coordinate data (using the ``gmx cphmd`` command).

To get help for ``analyse_titration.py``, run the script with ``-h`` flag:

```
	This script will analyse the titration trajectories of the protein. You have to provide the path to root .mdp file, prefix of the titration folders
	(pH), and pH range. The script will produce titration curve for each titratable grou.

	optional arguments:
	  -h, --help            show this help message and exit
	  -pH PHRANGE, --pHrange PHRANGE
	                        pH values at which we run simulations. Those can be provided in the form of range "a:b:d" or as a list "{a,b,c,..,}"
	  -i INPUT, --input INPUT
	                        Path to the folder with titration data
	  -f MDP, --mdp MDP     Input .mdp file
	  -p PREFIX, --prefix PREFIX
	                        Prefix name
```

As a result we get the following titration curves:

![Aspartic acid 42](2_1CVO_titration/tutorial_files/output_files/titration_group_1.png "Aspartic acid 42 titration, experimental pKa = 3.2")

![Aspartic acid 59](2_1CVO_titration/tutorial_files/output_files/titration_group_2.png "Aspartic acid 59 titration, experimental pKa = 3.2")

![Glutamic acid 17](2_1CVO_titration/tutorial_files/output_files/titration_group_3.png "Glutamic acid 17 titration, experimental pKa = 3.2")

![Histidine 4](2_1CVO_titration/tutorial_files/output_files/titration_group_4.png "Histidine 4 titration, experimental pKa = 3.2")

The titration curves look as they should. A more extensive sampling will lead to even better predictions. The obtained $`\text{p}K_\text{a}`$ values (3.7, 2.28, 3.98, 4.54 for ASP-42, ASP-59, GLU-4, HIS-4, respectively) are in good agreement with experimental values (3.2, <2, 4, 5.5 for ASP-42, ASP-59, GLU-4, HIS-4, respectively).

# References
[1]: https://doi.org/10.1021/bi00082a026 Solution structure of cardiotoxin V from Naja naja atra, Singhal AK. , Chien KY., Wu WG., and Rule GS., Biochemistry, 1993
[w]: https://doi.org/10.26434/chemrxiv-2022-n025t-v2  Scalable Constant pH Molecular Dynamics in GROMACS, Aho N., Buslaev P., Jansen A., Bauer P., Groenhof G., Hess B., ChemRxiv, 2022
