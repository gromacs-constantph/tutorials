import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, sys
import glob
import re

from scipy.optimize import curve_fit

def hendersonHasselbalch(x, pKa):
    return 1. / (10**(pKa - x) + 1)

def getPka(pHdata, sDeprotData):
    popt, pcov = curve_fit(hendersonHasselbalch, pHdata, sDeprotData, [5.0])
    return popt[0]

def getSdeprotSS(data):
    nprot   = np.sum(data < 0.2) + 0.
    ndeprot = np.sum(data > 0.8) + 0.
    return ndeprot / (nprot + ndeprot)

def getSdeprotMS_3s(data):
    n1 = np.sum(data[:,0] > 0.8) + 0.
    n2 = np.sum(data[:,1] > 0.8) + 0.
    n3 = np.sum(data[:,2] > 0.8) + 0.
    return 1 - n1 / (n1 + n2 + n3)

def pathVal(path):
    return int(path.split("-")[-2])

def getSdeprotDataFromFolder(folder, lambdaDict):
    files=glob.glob("{}/cph*xvg".format(folder))

    files = sorted(files, key = pathVal)
    
    lines = (line for line in open(files[0], 'rb')  if re.findall(b"^(?!@|#).*", line) )
    
    l = np.genfromtxt(lines)[:,1:]
    for file in files[1:]:
        lines = (line for line in open(file, 'rb')  if re.findall(b"^(?!@|#).*", line) )
        l = np.hstack((l, np.genfromtxt(lines)[:,1:]))
    
    res = {}
    # this should be generilized in the future
    # now we have only two cased - SS or MS with 3 sites
    for key, value in lambdaDict.items():
        if len(value) == 1:
            res[key] = getSdeprotSS(l[:,value[0]-1])
        if len(value) == 3:
            res[key] = getSdeprotMS_3s(l[:,np.array(value)-1])

    return res

def getAverageDict(listDict):
    n = len(listDict)
    resDict = listDict[0]
    keys = resDict.keys()
    for d in listDict[1:]:
        for key in keys:
            resDict[key] += d[key]

    for key in keys:
        resDict[key] /= n

    return resDict

def getSdeprotData(path, prefix, pHrange, lambdaDict):
    pHDict = {}
    for pH in pHrange:
        folders=sorted(glob.glob("{}/{}_{:.2f}/r_*".format(path, prefix, pH)))
        phSdeprot = []
        for folder in folders:
            phSdeprot.append(getSdeprotDataFromFolder(folder, lambdaDict))
        pHDict[pH] = getAverageDict(phSdeprot)
    return pHDict

def getNAA(mdpFile):
    with open(mdpFile, "r") as f:
        for l in f:
            if l.startswith("lambda-dynamics-number-atom-collections"):
                return int(l.split("=")[1])

def getLambdaDict(mdpFile):
    naa = getNAA(mdpFile)
    aaN, lgN = 1, 1
    lambdaDict = {}
    buffer = -1
    with open(mdpFile, "r") as f:
        for l in f:
            if l.startswith("lambda-dynamics-atom-set{}-initial-lambda".format(aaN)):
                nCL = len(l.split("=")[1].split())
                lambdaDict[aaN] = list(range(lgN, (lgN + nCL)))
                aaN += 1
                lgN += nCL
            if l.startswith("lambda-dynamics-atom-set") and "buffer-residue" in l and "yes" in l:
                for i in range(1,naa+1):
                    if l.startswith("lambda-dynamics-atom-set{}-buffer-residue".format(i)):
                        buffer = i
    if buffer > 0:
        lambdaDict.pop(buffer)

    return lambdaDict

def getTheoreticalData(pKa):
    pHPoints   = np.arange(1., 8.1, 0.1)
    sDepPoints = hendersonHasselbalch(pHPoints, pKa)
    return (pHPoints, sDepPoints)

def makePlot(pKa, pHData, sDeprotData, lID):
    plt.figure()
    plt.scatter(pHData, sDeprotData,  label = 'computed values')
    
    pHPoints, sDeprotPoints = getTheoreticalData(pKa)
    plt.plot(pHPoints, sDeprotPoints, label = 'predicted titration curve')
    
    plt.ylabel('S_deprot')
    plt.xlabel('pH')
    plt.xlim((1, 8))
    plt.ylim((0, 1))
    plt.title("titration of group {}, pKa = {:.2f}".format(lID, pKa))
    plt.legend(loc=2)
    plt.savefig("titration_group_{}.png".format(lID))

def getLambdaData(pHdict, lambdaIndex):
    lambdaData = []
    for i in pHdict.values():
        lambdaData.append(i[lambdaIndex])
    return lambdaData

def makePlots(pHdict):
    pHData = list(pHdict.keys())
    lambdaIDs = list(pHdict[pHData[0]].keys())
    nLambdas = len(lambdaIDs)
    for lID in lambdaIDs:
        lambdaData = getLambdaData(pHdict, lID)
        pKa = getPka(pHData, lambdaData)
        makePlot(pKa, pHData, lambdaData, lID)

if __name__ == "__main__":
    parser = argparse.ArgumentParser( \
            description='This script will analyse the titration trajectories\
            of the protein. You have to provide the path to root .mdp file,\
            prefix of the titration folders (pH), and pH range.\
            The script will produce titration curve for each titratable grou.')

    parser.add_argument('-pH', '--pHrange',required=True, type = str, \
                help = 'pH values at which we run simulations. \
                Those can be provided in the form of range "a:b:d" \
                or as a list "{a,b,c,..,}"')

    parser.add_argument('-i', '--input', required=False, type=str, \
                default = './', help = 'Path to the folder with titration data')

    parser.add_argument('-f','--mdp', required=True, type=str, \
                help='Input .mdp file')

    parser.add_argument('-p', '--prefix',required=False, type = str, \
                default = 'pH', help = 'Prefix name')


    args = parser.parse_args(args=None if sys.argv[1:] else ['--help'])

    if args.pHrange.startswith("{"):
        pH = np.array(list(float(x) for x in args.pHrange.strip()[1:-1].split(",")))
    else:
        pH = np.arange(float(args.pHrange.split(":")[0]), \
                                float(args.pHrange.split(":")[1]), \
                                float(args.pHrange.split(":")[2]))

    lambdaDict = getLambdaDict(args.mdp)
    phDict = getSdeprotData(args.input, args.prefix, pH, lambdaDict)
    makePlots(phDict)

