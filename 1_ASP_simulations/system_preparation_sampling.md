We start by creating a tripeptide structure file, adding water, ions, minimizing and equilibrating the system. The only difference is that we want to add 10 buffers, instead of 1 used at the parameterisation stage. This can be achieved by running 

```
	gmx genion -s 5_buf.tpr -p topol.top -o 5_buf.gro -np 10 -rmin 1.0 -pname BUF
```

10 buffer particles are added in order to have only small charges for the buffers during the actual constant pH simulation.
The second key difference is that we don't add position restraints to restrain the distance from buffers to titratable sites (this is achieved automatically by proper buffer parameterisation **cite second paper**).

In the similar manner as for parameterisation runs we should create an ``index.ndx`` file and add ``[ LAMBDA1 ]`` group for ASP atoms that change their charge upon protonation.