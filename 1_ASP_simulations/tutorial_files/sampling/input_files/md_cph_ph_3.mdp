
; POSITION RESTRAIN
define               = ; -DPOSRES -DPOSRES_BUF  ; Position restraints.

; RUN CONTROL
integrator           = md           
dt                   = 0.002         ; Time step (ps).
nsteps               = 50000000         ; 0.1 ns.      
comm-mode            = Linear        ; Remove center of mass translation.
comm-grps            = System ;Protein Non-Protein

; OUTPUT CONTROL
nstxout-compressed   = 10000         ; Write frame every 20.000 ps.

; NEIGHBOUR SEARCHING
cutoff-scheme        = Verlet        ; Related params are inferred by Gromacs.

; BOND PARAMETERS
constraints          = h-bonds       ; Constrain H-bond vibrations.
constraint_algorithm = lincs         ; Holonomic constraints.
lincs_iter           = 1             ; Related to accuracy of LINCS.
lincs_order          = 4             ; Related to accuracy of LINCS.

; ELECTROSTATICS
coulombtype          = PME           ; Use Particle Mesh Ewald.
rcoulomb             = 1.2           ; Berk: CHARMM was calibrated for 1.2 nm.
fourierspacing       = 0.14          ; Berk: set this to 0.14 for CHARMM.

; VAN DER WAALS
vdwtype              = cut-off       ; Twin range cut-off with nblist cut-off.
rvdw                 = 1.2           ; Berk: CHARMM was calibrated for 1.2 nm.
vdw-modifier         = force-switch  ; Berk: specific for CHARMM.
rvdw-switch          = 1.0           ; Berk: specific for CHARMM.

; TEMPERATURE COUPLING
tcoupl               = v-rescale    
tc-grps              = SYSTEM       
tau-t                = 0.5           ; Berk: change from 0.1 to 0.5.
ref-t                = 300           ; Reference temp. (K) (for each group).

; PRESSURE COUPLING
pcoupl               = Parrinello-Rahman
pcoupltype           = isotropic     ; Uniform scaling of box.
tau_p                = 5.0           ; Berk: better to change from 2.0 to 5.0.
ref_p                = 1.0           ; Reference pressure (bar).
compressibility      = 4.5e-05       ; Isothermal compressbility of water.
refcoord_scaling     = all           ; Required with position restraints.

; PERIODIC BOUNDARY CONDITION
pbc                  = xyz           ; To keep molecule(s) in box.

; CONSTANT PH
lambda-dynamics                                        = yes 
;lambda-dynamics-calibration        		               = yes
lambda-dynamics-simulation-ph                          = 3.0         
lambda-dynamics-lambda-particle-mass                   = 5.0          
lambda-dynamics-update-nst                             = 500        
lambda-dynamics-tau                                    = 2.0         
lambda-dynamics-number-lambda-group-types              = 2            
lambda-dynamics-number-atom-collections                = 2  
lambda-dynamics-charge-constraints                     = yes 
; lambda-dynamics-multistate-constraints                 = yes

lambda-dynamics-group-type1-name                       = ASP 
lambda-dynamics-group-type1-n-states                   = 1
lambda-dynamics-group-type1-state-0-charges            = -0.21 0.75 -0.55 -0.61 0.44
lambda-dynamics-group-type1-state-1-charges            = -0.28 0.62 -0.76 -0.76 0.0
lambda-dynamics-group-type1-state-1-reference-pka      = 3.65
lambda-dynamics-group-type1-state-1-dvdl-coefficients  = 487.794 -2123.99 3574.07 -3194.59 1539.43 -353.879 -555.935 45.9833

lambda-dynamics-group-type2-name                       = BUF 
lambda-dynamics-group-type2-n-states                   = 1
lambda-dynamics-group-type2-state-0-charges            = -0.5
lambda-dynamics-group-type2-state-1-charges            = 0.5
lambda-dynamics-group-type2-state-1-reference-pka      = 4.0
lambda-dynamics-group-type2-state-1-dvdl-coefficients  = -2391.04 8581.51 -10755.2 4646.51 382.852 -103.978 -1254.42 575.647

lambda-dynamics-atom-set1-name                         = ASP         
lambda-dynamics-atom-set1-index-group-name             = LAMBDA1   
lambda-dynamics-atom-set1-barrier		       = 7.5     
lambda-dynamics-atom-set1-initial-lambda               = 0.0                  
lambda-dynamics-atom-set1-charge-restraint-group-index = 1

lambda-dynamics-atom-set2-name                         = BUF                 
lambda-dynamics-atom-set2-index-group-name             = BUF 
lambda-dynamics-atom-set2-barrier		       = 0.0      
lambda-dynamics-atom-set2-initial-lambda               = 0.5
lambda-dynamics-atom-set2-buffer-residue               = yes          
lambda-dynamics-atom-set2-buffer-residue-multiplier    = 10
lambda-dynamics-atom-set2-charge-restraint-group-index = 1

