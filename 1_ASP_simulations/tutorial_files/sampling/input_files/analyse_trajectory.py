import numpy as np
from scipy.special import erf
import matplotlib.pyplot as plt
import argparse
import os, sys

# Calculate biasing potential
# We take the parameters from supplementary of Aho et al., Chemrxiv 2022
def getDWP(minl, maxl, dl):
	k = 4.7431 
	a = 0.0435
	b = 0.0027
	d = 3.75
	s = 0.3
	w = 1000
	r = 13.5
	m = 0.2019

	sInvSq = 1 / s / s
	aInvSq = 1 / a / a

	x = np.arange(minl, maxl+dl, dl)

	devRight    = x - 1 - b
	expRight    = np.exp(-0.5 * np.power(devRight, 2) * aInvSq)
	devLeft     = x + b
	expLeft     = np.exp(-0.5 * np.power(devLeft,  2) * aInvSq)
	devMid      = x - 0.5
	expMid      = np.exp(-0.5 * np.power(devMid,   2) * sInvSq)
	erfArgRight = r * (x - 1 - m)
	erfArgLeft  = r * (x + m)

	return np.array([x,-k * (expRight + expLeft) + d * expMid\
		   + w * 0.5 * (2 + erf(erfArgRight) - erf(erfArgLeft))]).T

# Calculate pH potential
# We take the parameters from supplementary of Aho et al., Chemrxiv 2022
def getPHP(minl, maxl, dl, pH, pKa):
	r = 13.5
	a = 0.0435

	k1 = 2.5 * r
	x0 = 2 * a

	x = np.arange(minl, maxl+dl, dl)

	# molar gas constant in kJ/mol
	R = 8.3145 / 1000
	# Temperature
	T = 300
	# prefactor 
	pref = R * T * np.log(10) * (pKa - pH)
	expT = np.exp(-2 * k1 * (x - x0)) if pH <= pKa else \
			np.exp(-2 * k1 * (x - 1 + x0))

	return np.array([x, pref * np.divide(1, 1 + expT)]).T

def getLambda(path, lambdaID):
	return np.genfromtxt(path, skip_header = 25)[:,lambdaID]

def getTheorDist(pot):
	# molar gas constant in kJ/mol
	R = 8.3145 / 1000
	# Temperature
	T = 300

	dist = np.array([pot[:,0], np.exp(-pot[:,1] / R / T)]).T
	dist[:,1] = dist[:,1] / np.sum(dist[:,1])
	return dist

def getCphDist(l, minl, maxl, dl):
	data = l
	hist, bins = np.histogram(data, np.arange(minl, maxl+dl, dl), (minl, maxl))
	hist = hist / np.sum(hist)
	return np.array([(bins[:-1] + bins[1:]) / 2, hist]).T



if __name__ == "__main__":
	parser = argparse.ArgumentParser( \
			description='This script will analyse the trajectories of\
			lambda coordinates for specific pH. You have to provide the pH,\
			amino acid pKa\
			number of buffer particles in the system, and path to the folder\
			with lamda-trajectories. The script assumes that lambda_1.dat\
			file has the trajectory of amino acid, and lambda_2.dat file\
			has the trajectory of buffer. The script will produce three plots:\
			(i) bising and pH potential terms; (ii) charge conservation; \
			(iii) distribution of amino acid \
			lambda coordinate compared to theoretical prediction.')

	parser.add_argument('-pH','--pH', required=True, type=float, \
			help='pH of the simulation')

	parser.add_argument('-pKa','--pKa', required=True, type=float, \
			help='pKa of the amino acid')

	parser.add_argument('-nb','--nBuf', required=True, type=int, \
			help='number of buffer particles')

	parser.add_argument('-d','--dir', required=True, type=str, \
			help='folder with the trajectory')

	parser.add_argument('-o', '--out', required=True, type=str, \
			help='prefix for output')

	args = parser.parse_args(args=None if sys.argv[1:] else ['--help'])

	dwp = getDWP(-0.15, 1.15, 0.01)
	ph = getPHP(-0.15, 1.15, 0.01, args.pH, args.pKa)
	totalp = np.array([dwp[:,0], dwp[:,1] + ph[:,1]]).T

	plt.figure()
	plt.plot(dwp[:,0], dwp[:,1], label = 'biasing potential')
	plt.plot(ph[:,0],  ph[:,1], label = 'pH potential')
	plt.plot(totalp[:,0],totalp[:,1], label = 'total potential')
	
	plt.ylabel('Potential (kJ/mol)')
	plt.xlabel('Lambda')
	plt.xlim((-0.15, 1.15))
	plt.ylim((-7.5, 25))
	plt.legend(loc=9)
	plt.savefig("{}_potentials.png".format(args.out))

	plt.figure()
	t  = getLambda(args.dir + "/cphmd-coord-1-2.xvg", 0)
	l1 = getLambda(args.dir + "/cphmd-coord-1-2.xvg", 1)
	l2 = getLambda(args.dir + "/cphmd-coord-1-2.xvg", 2)
	plt.plot(t * 0.001, l1 - 10 * l2)
	plt.title("Charge conservation")
	plt.xlabel('Time (ns)')
	plt.ylabel('L1 - Nbuf * L2')
	plt.ylim((-10, 10))
	plt.savefig("{}_charge.png".format(args.out))

	plt.figure()
	dist = getTheorDist(totalp)
	distC = getCphDist(l1, -0.15, 1.15, 0.01)
	plt.plot(dist[:,0], dist[:,1], label = "Theoretical distribution")
	plt.plot(distC[:,0], distC[:,1], label = "Calculated distribution")
	plt.title("Probability density")
	plt.xlabel('Lambda')
	plt.xlim((-0.15,1.15))
	plt.ylim((0,0.1))
	plt.legend(loc=9)
	plt.savefig("{}_distributions.png".format(args.out))
