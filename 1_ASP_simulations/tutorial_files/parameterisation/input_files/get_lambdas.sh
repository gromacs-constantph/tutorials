#!/bin/bash

#################################################
# get_lambdas.sh
#
#   Script iteratively calls gmx cphmd
#   to extract lambda coordinates and 
#   forces from .edr file obtained during
#   constant pH MD in GROMACS
#
# Written by: Pavel I. Buslaev, Ph.D.
#    Contact: pavel.i.buslaev@jyu.fi
#
#################################################

# collect lambda data
for (( i=0; i<25; i++ ))
do
    k=$(python<<<"print('%3.2f' % ($i*0.05-0.1))")
    l=$(python<<<"print('%3.2f' % (1.0-($i*0.05-0.1)))")
    cd p_${k}_${l}
    gmx cphmd -s run.tpr -e run.edr -dvdl -o
    cd ../
done

exit;
