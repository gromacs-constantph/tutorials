#!/bin/bash

#################################################
# start_param.sh
#
#   Script iteratively calls gmx grompp and
#   mdrun to set up callibration runs for 
#   constant pH MD in GROMACS for lambda
#   coordinates from -0.10 to 1.10 with
#   0.05 spacing
#
# Written by: Pavel I. Buslaev, Ph.D.
#    Contact: pavel.i.buslaev@jyu.fi
#
#################################################

# start calibration runs
for (( i=0; i<25; i++ ))
do
    k=$(python<<<"print('%3.2f' % ($i*0.05-0.1))")
    l=$(python<<<"print('%3.2f' % (1.0-($i*0.05-0.1)))")
    mkdir p_${k}_${l}
    cd p_${k}_${l}
    cp ../md_cph.mdp .
    sed -i "s/lambda-dynamics-atom-set1-initial-lambda               = 1.0/lambda-dynamics-atom-set1-initial-lambda               = $k/" md_cph.mdp
    sed -i "s/lambda-dynamics-atom-set2-initial-lambda               = 0.0/lambda-dynamics-atom-set2-initial-lambda               = $l/" md_cph.mdp
    gmx grompp -p ../topol.top -f md_cph.mdp -c ../8_npt.gro -r ../8_npt.gro -n ../index.ndx -o run.tpr -maxwarn 1
    gmx mdrun -deffnm run &
    cd ../
done

exit;

