First, we start by creating a tripeptide structure file. For that we can use the building functionality of Pymol, or any other software you prefer. We create ALA-ASP-ALA tripeptide with capped termini and save the coordinates into file ``0_asp.pdb``. We then change the ASP residue name to ``ASPT``. This will let GROMACS recognise ASP as a titratable residue.

Next, we use ``pdb2gmx`` command to create the topology for this structure.
	
> **Note**, that we are using the modified CHARMM36 force field for this tutorial. The force field folder and the corresponding ``residuetypes.dat`` file must be in the working directory, otherwise titratable residues won't be recognised by GROMACS.

```
	gmx pdb2gmx -f 0_asp.pdb -o 1_asp.gro -p -ignh -ter
```

The modified CHARMM36 force field (option 1), ``tip3p`` water model, and ``None`` for both terminus should be selected. As an output we obtain the ``1_asp.gro`` and ``topol.top`` files. We now need to create a box and add solvent to it:

```
	gmx editconf -f 1_asp.gro -o 2_box.gro -box 5 5 5
	gmx solvate -cp 2_box.gro -cs -p topol.top -o 3_solv.gro
```

Now we have the solvated tripeptide and it's time to add ions:

```
	gmx grompp -f ions.mdp -c 3_solv.gro -p topol.top -o 4_ions.tpr
	gmx genion -s 4_ions.tpr -p topol.top -o 4_ions.gro -neutral -conc 0.15
```

During the parameterisation we will run several simulations for different fixed $`\lambda`$-values for ASP. Thus, the charge of the tripeptide will be different in these runs, depending on the $`lambda`$-value. To avoid artefacts associated with a non-neutral simulation box, we will add one buffer particle in the box, which will compensate for the charge changes of the ASP tripeptide. Initially ($`\lambda=0`$) ASP is protonated and thus has zero charge. Thus, the initial charge of the buffer also should be zero. Upon deprotonation, ASP will get a negative charge and the buffer, therefore, will uptake positive charge with the same absolute value as ASP. This will keep the box neutral during all parameterisation runs with fixed $`\lambda`$. We add one buffer particle using ``genion`` utility:

```
	gmx grompp -f ions.mdp -c 4_ions.gro -p topol.top -o 5_buf.tpr
	gmx genion -s 5_buf.tpr -p topol.top -o 5_buf.gro -np 1 -rmin 1.0 -pname BUF
```

To avoid an accidental binding of the buffer particle to ASP during the parameterisation runs, we will use position restraints for both buffer and tripeptide. In the tripeptide, we will only fix the $`\text{C}_{\alpha}`$ atom of ASP, and enable sampling all possible directions of buffer-ASP interaction during the parameterisation runs. For the position restraints, we need to prepare a ``posre.itp`` file for the tripeptide as follows:

```
; In this topology include file, you will find position restraint
; entries for Ca atom ASP tripeptide

[ position_restraints ]
; atom  type      fx      fy      fz
    19     1  1000  1000  1000
```
> **NOTE:** Check that the index in the ``posre.itp`` file corresponds to ASP $`\text{C}_{\alpha}`$ atom in ``topol.top``.

To apply the position restraints we use ``-DPOSRES -DPOSRES_BUF`` flags in $`\tt{.mdp}`$ files for minimisation, equilibration, and calibration runs. 

We then use standard GROMACS to minimize and equilibrate the system:

```
	gmx grompp -f min.mdp -c 5_buf.gro -p topol.top -o 6_min.tpr -r 5_buf.gro  
	gmx mdrun -v -deffnm 6_min 
	gmx grompp -f nvt.mdp -c 6_min.gro -p topol.top -o 7_nvt.tpr -r 6_min.gro 
	gmx mdrun -v -deffnm 7_nvt 
	gmx grompp -f npt.mdp -c 7_nvt.gro -p topol.top -o 8_npt.tpr -r 7_nvt.gro
	gmx mdrun -v -deffnm 8_npt  
```

After the equilibration is completed, we are ready to start the actual calibration runs with fixed $`\lambda`$-values.