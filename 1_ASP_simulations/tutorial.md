# Constant pH MD of single amino acid

In this tutorial we will simulate the tripeptide ALA-ASP-ALA using GROMACS constant pH code [1]. First, we will obtain the polynomial fit for $`V^{\text{MM}}`$ correction potential (in Parameterization). Next, we will use those coefficients to run the simulations of the tripeptide at $`\text{pH}=3`$ and $`\text{pH}=4`$ (in Constant pH MD), and finally we will analyse the results of constant pH runs (in Analysis of protonation states).

> **NOTE** The parameterization procedure has to be run only once for a specific titratable group, such as aspartic acid. This is done for a model system (titratable group alone in water), for which the reference pKa value is also known. Hence, if the system for constant pH MD contains for example multiple Asp residues, the same coefficients from model parameterization of Asp in water will be used.

# Parameterisation
In this part we will obtain the ASP coefficients for $`V^{\text{MM}}`$. All the input files needed for this task could be found in ``tutorial_files/parameterisation/input_files`` folder. We recommend to obtain all the output files yourself, but if you are limitted on the computational resources, all the output files could be found in ``tutorial_files/parameterisation/output_files`` folder.

## System preparation

System preparation is similar to standard simulations, but the details can be found [here](https://gitlab.com/gromacs-constantph/tutorials/-/blob/main/1_ASP_simulations/system_preparation.md)

## Thermodynamic integration for reference state

To start the parameterisation, we first need to prepare the $`\tt{.mdp}`$ file for constant pH simulations. In ``md_cph.mdp`` file there is a constant pH related section:

```
	; CONSTANT PH
	lambda-dynamics                                        = yes
	lambda-dynamics-calibration                            = yes
	lambda-dynamics-simulation-ph                          = 4.0
	lambda-dynamics-lambda-particle-mass                   = 5.0
	lambda-dynamics-update-nst                             = 500
	lambda-dynamics-tau                                    = 2.0
	lambda-dynamics-number-lambda-group-types              = 2
	lambda-dynamics-number-atom-collections                = 2
	lambda-dynamics-charge-constraints                     = yes

	lambda-dynamics-group-type1-name                       = ASP
	lambda-dynamics-group-type1-n-states                   = 1
	lambda-dynamics-group-type1-state-0-charges            = -0.21 0.75 -0.55 -0.61 0.44
	lambda-dynamics-group-type1-state-1-charges            = -0.28 0.62 -0.76 -0.76 0.0
	lambda-dynamics-group-type1-state-1-reference-pka      = 3.65
	lambda-dynamics-group-type1-state-1-dvdl-coefficients  = 0

	lambda-dynamics-group-type2-name                       = BUF
	lambda-dynamics-group-type2-n-states                   = 1
	lambda-dynamics-group-type2-state-0-charges            = 1.0
	lambda-dynamics-group-type2-state-1-charges            = 0.0
	lambda-dynamics-group-type2-state-1-reference-pka      = 4.0
	lambda-dynamics-group-type2-state-1-dvdl-coefficients  = 0

	lambda-dynamics-atom-set1-name                         = ASP
	lambda-dynamics-atom-set1-index-group-name             = LAMBDA1
	lambda-dynamics-atom-set1-barrier                      = 0.0        
	lambda-dynamics-atom-set1-initial-lambda               = 1.0

	lambda-dynamics-atom-set2-name                         = BUF
	lambda-dynamics-atom-set2-index-group-name             = BUF
	lambda-dynamics-atom-set2-barrier                      = 0.0        
	lambda-dynamics-atom-set2-initial-lambda               = 0.0
	lambda-dynamics-atom-set2-buffer-residue               = yes
	lambda-dynamics-atom-set2-buffer-residue-multiplier    = 1
```

Please, check out the [manual](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md) for the detailed description of the options. In brief, we first set up general constant pH parameters, specifying that we use constant pH for calibration (``lambda-dynamics-calibration = yes``). Next, we introduce 2 ``group-types``, one for ASP, and one for BUF. The most important here is to present the charges for different protonated states. For ASP these charges can be obtained from force field parameters. We need to provide the charges that are different in different protonation states. We can extract this information from the ``ASPT`` and ``ASP`` entries of ``charmm36-mar2019-m6-buf.ff/merged.rtp`` file:

```
[ ASPT ] ; titratable ASP
 [ atoms ]
           N   NH1   -0.470  0
          HN     H    0.310  1
          CA  CT1n    0.070  2
          HA   HB1    0.090  3
          CB  CT2n   -0.210  4
         HB1   HA2    0.090  5
         HB2   HA2    0.090  6
          CG   CDn    0.750  7
         OD1    OB   -0.550  8
         OD2  OH1n   -0.610  9
         HD2     H    0.440 10
           C     C    0.510 11
           O     O   -0.510 12
```
```
[ ASP ]
  [ atoms ]
            N   NH1   -0.470  0
           HN     H    0.310  1
           CA   CT1    0.070  2
           HA   HB1    0.090  3
           CB  CT2A   -0.280  4
          HB1   HA2    0.090  5
          HB2   HA2    0.090  6
           CG    CC    0.620  7
          OD1    OC   -0.760  8
          OD2    OC   -0.760  9
            C     C    0.510 10
            O     O   -0.510 11
```

We see, that for atoms ``CB``, ``CG``, ``OD1``, ``OD2``, and ``HD2`` the charges are different in two protonation states and we provide those charges to ``lambda-dynamics-group-type1-state-0-charges`` and ``lambda-dynamics-group-type1-state-1-charges`` entries of ``md_cph.mdp`` file, respectively (check [manual](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md) for more details). For the ``BUF`` group type, we set the charge limits from 0 to 1 to neutralize the charge of upon deprotonating ASP, as discussed above.

Next, we specify ``atom-set`` entries in the ``md_cph.mdp`` file. The most important thing here, is to specify the correct group from index file. Thus, we need to have a group ``[ LAMBDA1 ]``, where indices of ``CB``, ``CG``, ``OD1``, ``OD2``, and ``HD2`` atoms are specified, and a group ``[ BUF ]``, where the index of buffer is specified. We first create the index file:
```
	gmx make_ndx -f 8_npt.gro 
```
Next, we can manually add a group ``[ LAMBDA1 ]`` with the correct indices to the ``index.ndx`` file.

Now we are ready to start the parametrisation runs. We will start 25 simulations for ASP with fixed $`\lambda`$-values, ranging from -0.1 to 1.1 with 0.05 step. For this we use a simple bash script ``start_param.sh``. The script will automatically create separate folders for output associated with each fixed $`\lambda`$, change the initial $`\lambda`$-coordinates for ASP and buffer accordingly, and run ``grompp`` and ``mdrun`` commands for every $`\lambda`$. You can modify the script to use it on the HPC cluster.

As a result after running the script, you should have 25 directories named ``p_-0.10_1.10``, ``p_-0.05_1.05``, ... , ``p_1.10_-0.10``. In each directory there should be standard GROMACS output files (e.g. $`\tt{.xtc}`$). The trajectories, velocities and forces for $`\lambda`$-coordinates associated with ASP and buffer are stored in $`\tt{.xtc}`$ file. To extract coordinates and forces we use `gmx cphmd` command:

```
	gmx_mpi cphmd -s run.tpr -e run.edr -dvdl -o
```
for each parameterisation directory. To run this automatically for all the folders we can use a simple bash script ``get_lambdas.sh``. As a result we will obtain two files for each directory: ``cphmd-coord-1-2.xvg`` with lambda coordinates, and ``cphmd-dvdl-1-2.xvg``with lambda forces. Let's take a closer look at the coordinate file first:

```
@    title "Constant pH Lambda Coordinate for Lambda 1 - 2"
@    xaxis  label "Time (ps)"
@    yaxis  label "Lambda Coordinate"
@TYPE xy
@ view 0.15, 0.15, 0.75, 0.85
@ legend on
@ legend box on
@ legend loctype view
@ legend 0.78, 0.8
@ legend length 2
@ s0 legend "Atom Collection ASP ASP, index (1)"
@ s1 legend "Atom Collection BUF BUF, index (2)"
  0.0000      0.0000      1.0000
  1.0000      0.0000      1.0000
  2.0000      0.0000      1.0000
  3.0000      0.0000      1.0000
  4.0000      0.0000      1.0000
  5.0000      0.0000      1.0000
  6.0000      0.0000      1.0000
  7.0000      0.0000      1.0000
  8.0000      0.0000      1.0000
  9.0000      0.0000      1.0000
 10.0000      0.0000      1.0000
```

This is a standard $`\tt{.xvg}`$ file, with three columns: the first column corresponds to time, the second and the third to $`\lambda`$-coordinates of ASP and buffer, respectively. Since we used constant pH MD in parameterisation mode, the $`\lambda`$-s are fixed. However, the output is organised in the exact same way for normal constant pH runs. The force file has a very similar format, but it has columns that correspond to forces acting of $`\lambda`$ instead of coordinates:

```
@    title "Constant pH Lambda dvdl for Lambda 1 - 2"
@    xaxis  label "Time (ps)"
@    yaxis  label "Lambda dvdl"
@TYPE xy
@ view 0.15, 0.15, 0.75, 0.85
@ legend on
@ legend box on
@ legend loctype view
@ legend 0.78, 0.8
@ legend length 2
@ s0 legend "Atom Collection ASP ASP, index (1)"
@ s1 legend "Atom Collection BUF BUF, index (2)"
  0.0000      9.5740    -39.5189
  1.0000    104.7481    -40.4724
  2.0000     36.6709    -79.0216
  3.0000     19.4858    -71.1761
  4.0000    -28.5333    -25.7994
  5.0000     20.8947    -34.1636
  6.0000     77.1559    -118.9091
  7.0000     27.4140    -60.2268
  8.0000     49.7603     13.7130
  9.0000     24.4335    -45.2074
 10.0000    134.7642      9.8935

```

To obtain the best polynomial fit for $`V^{\text{MM}}`$ for ASP, we need to calculate the average $`dV^{\text{MM}}/d\lambda`$ for each fixed $`\lambda`$ in the parameterisation run (column Lambda dvdl), and then obtain the polynomial coefficients for the optimal fit of these averages as a function of $`\lambda`$. 
This procedure can be done with *Mathematica* notebook available as the [supporting information](https://chemrxiv.org/engage/chemrxiv/article-details/61efbf52360c8410eeaa2d24) for constant pH manuscript [1]. The notebook contains all the important details and thus, we don't focus on the fitting procedure here. For the following part of the tutorial we will use $`7^{\text{th}}`$ order fits. The polynomial fit should lead to a curve which is close to:

```math
	487.794 x^7 - 2123.99 x^6 + 3574.07 x^5 - 3194.59 x^4 + 1539.43 x^3 - 353.879 x^2 - 555.935 x + 45.9833
```

# Constant pH MD
In this part we will perform constant pH MD simulations of ASP tripeptide with the fitting coefficients obtained at the previous part. We will need coefficients both for ASP and for the buffer particle. Coefficients for buffers can be obtained with the exact same procedure we applied for ASP.

> **NOTE** The current version of the code doesn't allow to have different initial charges for molecules of the same type. Since we will need two buffers with opposite charges to actuall obtain $`V^{\text{MM}}`$ for buffer particle, we can hack this around by adding a pseudotype BUF1 to the ions.itp file with the exact same parameters as BUF type have. This allows us to have two buffers with two different molecule types.

In this part of the tutorial we will use the following coefficients for ASP (you can use the ones you obtained in the previous part):

```
	487.794 -2123.99 3574.07 -3194.59 1539.43 -353.879 -555.935 45.9833
```
and for buffer particles (you can obtain you own coefficients) we will use:

```
	-2391.04 8581.51 -10755.2 4646.51 382.852 -103.978 -1254.42 575.647
```

All the input files needed for this part could be found in ``tutorial_files/sampling/input_files`` folder. We recommend to obtain all the output files yourself, but if you are limitted on the computational resources, all the output files could be found in ``tutorial_files/sampling/output_files`` folder.

## System preparation for constant pH simulations
System preparation procedure is very similar to one performed in the ``Parameterisation`` part of this tutorial but some details that differ can be found [here](https://gitlab.com/gromacs-constantph/tutorials/-/blob/main/1_ASP_simulations/system_preparation_sampling.md) . 

## Constant pH simulations
Now we are all ready to start constant pH simulations. We will start two simulations for pH 3 and pH 4. For these we again need to provide correct $`\tt{.mdp}`$ options. To set the value for pH, we simply specify the correct pH in the corresponding $`\tt{.mdp}`$ entry (check ``md_cph_ph_3.mdp`` and ``md_cph_ph_4.mdp`` files):

```
	lambda-dynamics                                        = yes
	lambda-dynamics-calibration                            = no
	lambda-dynamics-simulation-ph                          = 3.0
	lambda-dynamics-lambda-particle-mass                   = 5.0
	lambda-dynamics-update-nst                             = 500
	lambda-dynamics-tau                                    = 2.0
	lambda-dynamics-number-lambda-group-types              = 2
	lambda-dynamics-number-atom-collections                = 2
	lambda-dynamics-charge-constraints                     = yes
```

Note, that we also have to set ``lambda-dynamics-calibration`` to ``no`` (this also can be achieved by not specifying this entry at all, check [manual](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md) for the details).

The other important thing is to specify the barrier height of the biasing potential for titratable group (check [manual](https://gitlab.com/gromacs-constantph/constantph/-/blob/main/constant-ph.md)). This is needed to avoid sampling of nonphysical states.

```
	lambda-dynamics-atom-set1-name                         = ASP
	lambda-dynamics-atom-set1-index-group-name             = LAMBDA1
	lambda-dynamics-atom-set1-barrier                      = 7.5
	lambda-dynamics-atom-set1-initial-lambda               = 0.0
	lambda-dynamics-atom-set1-charge-restraint-group-index = 1
```

We also need to set the correct charge range for buffer ``group-type`` entry:

```
	lambda-dynamics-group-type2-name                       = BUF
	lambda-dynamics-group-type2-n-states                   = 1
	lambda-dynamics-group-type2-state-0-charges            = -0.5
	lambda-dynamics-group-type2-state-1-charges            = 0.5
	lambda-dynamics-group-type2-state-1-reference-pka      = 4.0
	lambda-dynamics-group-type2-state-1-dvdl-coefficients  = -2391.04 8581.51 -10755.2 4646.51 382.852 -103.978 -1254.42 575.647
```

Now we are all ready to start constant pH MD of tripeptide ASP at pH 3 and pH 4. Simply run ``gmx grompp`` and ``gmx mdrun`` with desired options on your machine to start simulations.

## Analysis of protonation states
When the simulations are done, we are ready to analyze the results. First, we need to extract lambda trajectories from $`\tt{.edr}`$ files using `gmx cphmd` command:

```
	gmx_mpi cphmd -s run.tpr -e run.edr -o
```

Our analysis is based on ``analyse_trajectory.py`` script and will consist of 3 parts: (i) we take a look at biasing $`V^{\text{bias}}`$ and pH dependent $`V^{\text{pH}}`$ terms of potential; (ii) we check that the total charge of the system is indeed conserved in our simulations; (iii) we compare the distirbution of ASP $`\lambda`$-coordinate with theoretical prediction.

All the analysis is done by ``analyse_trajectory.py`` script. You can get help for the script by running ``python analyse_trajectory.py`` or ``python analyse_trajectory.py -h``. Here is a brief description of the script:

```
This script will analyse the trajectories of lambda coordinates for specific pH. You have to provide the pH, amino acid pKa number of buffer particles in the system, and path to the folder with lamda-trajectories. The script assumes that lambda_1.dat file has thetrajectory of amino acid, and lambda_2.dat file has the trajectory of buffer. The script will produce three plots: (i) bising and pH potential terms; (ii) charge conservation; (iii) distribution of amino acid lambda coordinate compared to theoretical prediction.

	optional arguments:
  		-h, --help            	show this help message and exit
  		-pH PH, --pH PH       	pH of the simulation
  		-pKa PKA, --pKa PKA   	pKa of the amino acid
  		-nb NBUF, --nBuf NBUF 	number of buffer particles
  		-d DIR, --dir DIR     	folder with the trajectory
  		-o OUT, --out OUT     	prefix for output
```

We start by analysing the trajectory for $`\text{pH} = 3`. You need to provide path to the directory with the corresponding simulation data, set the $`\text{pH}`$ and $`\text{p}K_{\text{a}}`$ for the aspartic acid (3.65), specify number of buffers, and select the prefix for output files. As a result, you get 3 $`\tt{.png}`$ figures: ``{prefix}_potentials.png``, ``{prefix}_charge.png``, and ``{prefix}_distributions.png``.

For the potentials you should get a similar figure to this:

![ph3 potentials](1_ASP_simulations/tutorial_files/sampling/output_files/ph3_potentials.png "Potentials for pH = 3")

Next, we want to check that the total charge of the system is conserved during our simulations. If we check the $`\tt{.mdp}`$ file for ASP and buffer residue types definitions, we can see that for ASP $`\lambda = 0`$ corresponds to protonated state
```
	lambda-dynamics-group-type1-state-0-charges            = -0.21 0.75 -0.55 -0.61 0.44
```
and $`\lambda = 1`$ corresponds to deprotonated state
```
	lambda-dynamics-group-type1-state-1-charges            = -0.28 0.62 -0.76 -0.76 0.0
```
leading to the total protonated charge of $`q_p = -0.18`$, and total depronated charge of $`q_d = -1.18`$. For buffer, the total charge for $`\lambda = 1`$ is $`q^b_1 = 0.5 N^{\text{buffer}}`$ and for $`\lambda = 0`$: $`q^b_0 = -0.5 N^{\text{buffer}}`$, where $`N^{\text{buffer}}`$ is number of buffer in the system (10 in our simulations, check ``buffer-residue-multiplier`` entry of the $`\tt{.mdp}`$ file). Thus, the total charge of ASP is:

```math
	q^{\text{ASP}} = (1 - \lambda) q_p + \lambda q_d
```
And the total charge of buffers is:

```math
	q^{\text{buffer}} = (1 - \lambda^b) q^b_0 + \lambda^b q^b_1

```

Since the charges of the rest of the atoms in the system don't change when $`\lambda`$-coordinates are changing, we only need to check that $`q^{\text{ASP}} + q^{\text{buffer}}`$ is constant. If we use the equations for the ASP and buffer charges we get:

```math
	q^{\text{ASP}} + q^{\text{buffer}} = -0.18 + \lambda(0.18-1.18) - 0.5 N^{\text{buffer}} +\lambda^b N^{\text{buffer}} = -0.18 - 0.5 N^{\text{buffer}} + \left[ \lambda^b N^{\text{buffer}} - \lambda\right]
```

Thus, the total charge is fixed only if $`\lambda - \lambda^b N^{\text{buffer}}`$ is fixed. This expression is plotted in ``{prefix}_charge.png`` and is indeed constant:

![ph3 charge](1_ASP_simulations/tutorial_files/sampling/output_files/ph3_charge.png "Charge conservation for pH = 3")

Finally, we would like to check if the obtained distribution of ASP $`\lambda`$-coordinate from our constant pH simulation agrees well with the theoretical prediction. To obtain the theoretical prediction we can use Boltzman's inversion of the underlying potential for $`\lambda`$-coordinate $`V = V^{\text{pH}} + V^{\text{bias}}`$:
```math
	p = \frac{1}{Z_0} \exp\left(-\frac{V}{RT}\right)
```

The ``analyse_trajectory.py`` script does this inversion, and produces the probability density plots for theoretical and computed distributions. They are in almost perfect agreement:

![ph3 distribution](1_ASP_simulations/tutorial_files/sampling/output_files/ph3_distributions.png "Lambda distributions for pH = 3")

For $`\text{pH}=4`$ we perform the exact same analysis. Note the difference in the shape of $`V^{\text{pH}}`$:

![ph4 potentials](1_ASP_simulations/tutorial_files/sampling/output_files/ph4_potentials.png "Potentials for pH = 4")

The distributions are again in good agreement:

![ph4 distribution](1_ASP_simulations/tutorial_files/sampling/output_files/ph4_distributions.png "Lambda distributions for pH = 4")

If the results you obtained are significantly different from what is shown in the tutorial, please carefully check you input files. If you think that you did everything right, but the results are not what you expected, please contact one of the Constant pH developers and share your input data.

# References
[1]: https://doi.org/10.26434/chemrxiv-2022-n025t-v2  Scalable Constant pH Molecular Dynamics in GROMACS, Aho N., Buslaev P., Jansen A., Bauer P., Groenhof G., Hess B., ChemRxiv, 2022
